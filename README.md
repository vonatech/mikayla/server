# Requirements
- MongoDB
- Redis
- Composer
- HTTPD Apache, 2.4 preferred
- PHP 7.0+
- PHP mcrypt and curl extensions

# License
Read the license please : [here](https://gitlab.com/muonium/mikayla/server/raw/master/LICENSE)

# Installation

TODO LATER

# Documentation
You can find our documentation [here](). (TODO : Insert link later)
